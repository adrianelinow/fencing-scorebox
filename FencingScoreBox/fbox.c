//------------------------------------------------
// Affordable Fencing Box
//
// Microcontroller code
//
// Coded I/O for ATMEL AVR processors April 27, 2006  PG
// Processor clock is 8MHz, internal timer
// Timer 0 is the global timer, ticks (interrupts) every 0.1 ms.
// Rewrote foil state machine, added condition to turn off buzzer when foil
// inputs are not connected and added microbreak handling May 20, 2006  PG
// Rewrote sabre state machine June 5, 2006
// Problem: In play, box doesn't record a hit (foil) when player holds
// blade/guard October 11
// Can't reproduce problem by connecting high resistance between guard and
// lame
// Increased wait time for foil hit from 3us to 6us October 14
// This seems to fix problem  - tested on October 18
// Added code to keep light on for 0.0001*LIGHTDELAY seconds after buzzer
// stops, Dec. 10 2006
// Improved debounce for mode select switch 20 Jan. 2007
// Added mode to select buzzer fequency, (both mode switches jumpered),
// put initialization, mode selection, etc. in separate functions 23 Jan. 2007
// Lowered resistance for sabre April 2007
// Added input (PD6) for external timer blocking, active low (21 Nov. 2008)
// Added output (PD5) to stop timer on hit, and 3rd segment for FES (PD4) (20 Jan. 2009)
// Also has added code for idle power down - to remove, search for sleeptimer and comment out
// Changed sabre lockout time to 170 ms. in fbox.h 
// Also changed initialization sequence - now white first - to show which ones are updated (Jan. 24 2016)
// Last updated: Jan. 24,2016  P.G
//------------------------------------------------

#include "fbox.h"

// Interrupt-driven timer functionality
volatile uint16_t timer[2];
volatile uint8_t timerOff[2];
volatile uint8_t parrytimer;
volatile uint16_t sleeptimerclock; //- added to put box to sleep after long idle
volatile uint8_t sleeptimer;       //- added to put box to sleep after long idle

// Enumerated types for players
//- enum Player {RED=0, GREEN} player;

#define RED 0
#define GREEN 1

enum BoxMode {modeEpee=0, modeFoil, modeSabre } mode;

// Enumerated types for state machines
enum State { Init = 0, Transient, CheckTouch, Hit, Throttle } fence[2];
enum SabreParryCheck { sParryInit = 0, sParry, sParryBlock } sabreParry;

// Other variables for state machines
uint8_t fOntarget[2], microbreak[2], oldparry, PARRY_INTERRUPT_COUNT, THROTTLE, BLOCK, BUZZ, NO_RED_HIT=TRUE, NO_GREEN_HIT=TRUE, EXTEND_LIGHT=FALSE;
// uint8_t ee_buzz __attribute__((section(".eeprom"))) = 240;  // 250 = 2000 Hz, 200 = 2500 Hz, 100 = 5000 Hz
uint8_t EEMEM ee_buzz;

uint8_t freq;

// Function prototypes; descriptions of the functions are
// found next to the definitions.

void init ( void );
void initialize_ports( void);
void test_output( void );
void restartStates ( void );
inline void buzzer_on(void);
inline void buzzer_off(void);
void set_buzz ( void );
void set_mode( void );
void block( void );
void power_down_sleeptimer( void );  //- for automatic shutoff

// Individual weapon state machine progression functions
void epeeProcess ( void );
void foilProcess ( void );
void sabreProcess ( void );

// Main function
int main() {
	uint8_t test;

// set up system clock, timers, buzzer, DDRs for primary putputs
	mode = modeEpee;  // default mode (quiet, no lights)
	init();

// test primary outputs -- lights and buzzer
	test_output();

// Infinite loop which handles state machine progressions
	while (TRUE) {

// check to see if external timer is blocking - pin low
		test = TIMER_PIN & _BV(TIMER_BLOCK);
		if (!test) block(); 

// Retrieve the new mode if it has changed (default is epee --- quiet)
		MODE_IN_PORT |= _BV(MODE_FES_PIN) | _BV(MODE_STATE_PIN);
		test = MODE_IN_PIN & _BV(MODE_FES_PIN);
		if (!test) set_mode();

		// Progress the state machine according to the mode
		switch ( mode ) {

			case modeEpee:	
				epeeProcess();
				break;

			case modeFoil:	
				foilProcess();
				break;

			case modeSabre:
				sabreProcess();
				break;
		}

	 if (sleeptimer > 150) power_down_sleeptimer();  // gives a 15 minute approx. power down
//-	 if (sleeptimer > 20) power_down_sleeptimer();  //- for test - 10 is about 1 minute

	sleep_mode();    // sleep until timer 0 interrrupts
	}
}

// Process foil events and progress the state machine as required.
// normally closed switch - a hit is recognized when the switch is
// open 14 +-1 ms.
// To detect A hit, the blade wire is powered, and the guard grounded,
// pulling the blade wire low. When the switch opens, the blade wire goes high.
// To detect if a hit is on target, the opponents jacket is grounded, and
// the jacket pulls the blade wire low (if the jacket resistance < 500 ohms).
// Subsequent hits by the opponent are valid only for the next 300 +-25 ms.
// Blocking is detected as for a hit, but between the guard and players jacket.
//
void foilProcess ( void ) {
	uint8_t test, player, touch[2], offtarget[2];
if (!THROTTLE)
 {
	INPUT_DDR  =  _BV(JACKET_GREEN); // set JACKET_GREEN to output, else input
	INPUT_PORT = _BV(GUARD_GREEN);   // set pullup, all other inputs to high-Z

	BLADE_PWR_DDR  = ~(_BV(BLADE_RED_PWR)|_BV(BLADE_GREEN_PWR));

	GUARD_PWR_DDR  &= ~(_BV(GUARD_RED_PWR)|_BV(GUARD_GREEN_PWR)); // set to inputs
	BLADE_PWR_PORT = ~(_BV(BLADE_RED_PWR)|_BV(BLADE_GREEN_PWR));
	GUARD_PWR_PORT &= ~(_BV(GUARD_RED_PWR)|_BV(GUARD_GREEN_PWR));// tristate

	// Check for GREEN blocking

	// Turn on GREEN guard power, set jacket to ground, and test blade
	// (or guard) input ---  tri-state everything else

	GUARD_PWR_DDR  |= _BV(GUARD_GREEN_PWR);
	GUARD_PWR_PORT |=  _BV(GUARD_GREEN_PWR); // output_high ( GUARD_GREEN_PWR );
	
	_delay_us(3);
// GUARD_GREEN_PWR output high, JACKET_GREEN output low, GUARD_GREEN input

	test = INPUT_PIN & _BV(GUARD_GREEN);  // input(GUARD_GREEN)
	if ( test )  {   // GREEN is not blocking
		LIGHT_PORT &= ~_BV(YELLOW_GREEN); // turn off yellow lamp
	}
	else {        // GREEN is blocking
		LIGHT_PORT |= _BV(YELLOW_GREEN);   //turn on yellow lamp
	}

	INPUT_DDR  &= ~_BV(JACKET_GREEN);   // set jacket to high-Z
	GUARD_PWR_DDR  &= ~_BV(GUARD_GREEN_PWR); // turn off GUARD_GREEN_PWR
	GUARD_PWR_PORT &= ~_BV(GUARD_GREEN_PWR);  // tristate

	// Check for hits - turn on power RED's foil
	// Disable player GREEN's weapon (already done), check for hits on him.
	// Ground GREEN's guard if not blocking
	// first see if tip is depressed - ground guard, and test input
	
	BLADE_PWR_DDR |= _BV(BLADE_RED_PWR);
	BLADE_PWR_PORT |= _BV(BLADE_RED_PWR);
	INPUT_DDR   =  _BV(GUARD_RED);  // ground RED guard
	INPUT_PORT = _BV(BLADE_RED_IN);  // set pullup

	if (test) INPUT_DDR   |= _BV(GUARD_GREEN);	// ground GREEN guard if not blocking
	_delay_us(6);
// BLADE_RED_PWR output high, GUARDS RED & GREEN output low, BLADE_RED_IN input
	
	touch[RED]  = INPUT_PIN & _BV(BLADE_RED_IN);	// input(BLADE_RED_IN)

	if (touch[RED]) {          // a hit - on or off target
		// test for valid hit - ground green jacket, test blade
		INPUT_DDR  =  _BV(JACKET_GREEN);
		INPUT_PORT &= ~_BV(JACKET_GREEN);      // Set GREEN jacket to output 0 (ground)		
		_delay_us(3);
		offtarget[RED] = INPUT_PIN & _BV(BLADE_RED_IN);// input(BLADE_RED)
	}
	else sleeptimer=0;     //- for shutdown

	BLADE_PWR_DDR &= ~_BV(BLADE_RED_PWR);
	BLADE_PWR_PORT &= ~_BV(BLADE_RED_PWR); // Red blade set to high-Z

// JACKET_GREEN output low, BLADE_RED_IN input
//   --- RED inputs finished ---
	
	// Check for RED blocking

	// Turn on RED guard power, set jacket to ground, and test blade
	// (or guard) input --- tri-state everything else

	GUARD_PWR_DDR  |= _BV(GUARD_RED_PWR);
	GUARD_PWR_PORT |=  _BV(GUARD_RED_PWR); // power to (GUARD_RED_PWR);
	
	INPUT_DDR  = _BV(JACKET_RED);
	INPUT_PORT = _BV(GUARD_RED);    // set pullup on GUARD_RED input
					// Set RED jacket to output 0 (ground)
	_delay_us(3);
// GUARD_RED_PWR output high, JACKET_RED output low, GUARD_RED input

	test = INPUT_PIN & _BV(GUARD_RED);  // input(GUARD_RED)
	if ( test )  {
		//  not blocking; turn off the yellow lamp.
		LIGHT_PORT &= ~_BV(YELLOW_RED);  // output_low(YELLOW_RED)
	}
	else {        //  blocking
		LIGHT_PORT |= _BV(YELLOW_RED);  // turn on yellow lamp
	}

	INPUT_DDR  &= ~_BV(JACKET_RED);   // set jacket to high-Z
	GUARD_PWR_DDR  &= ~_BV(GUARD_RED_PWR);  // turn off GUARD_RED_PWR
	GUARD_PWR_PORT &= ~_BV(GUARD_RED_PWR);   // tristate GUARD_RED_PWR

	// Check for hits - turn on power GREEN's foil
	// Disable player RED's weapon (already done), check for hits on him.
	// first see if tip is depressed - ground guard, and test input

	BLADE_PWR_DDR |= _BV(BLADE_GREEN_PWR);
	BLADE_PWR_PORT |= _BV(BLADE_GREEN_PWR);
	INPUT_DDR   =  _BV(GUARD_GREEN);
	INPUT_PORT  =  _BV(BLADE_GREEN_IN); // set pullup on BLADE_GREEN
					  // set GUARD_GREEN output low
	if (test) INPUT_DDR   |= _BV(GUARD_RED);	// ground RED guard if not blocking
	_delay_us(6);
// BLADE_GREEN_PWR output high, GUARDS GREEN & RED output low, BLADE_GREEN_IN input
	
	touch[GREEN]  = INPUT_PIN & _BV(BLADE_GREEN_IN);// input(BLADE_GREEN_IN)

	if (touch[GREEN]) {          // a hit - on or off target
			// test for valid hit - ground RED jacket, test blade
		INPUT_DDR  =  _BV(JACKET_RED);  // set JACKET_RED output low
		_delay_us(3);

		offtarget[GREEN] = INPUT_PIN & _BV(BLADE_GREEN_IN);  // input(BLADE_GREEN)
	}
	else sleeptimer=0;     //- for shutdown

// BLADE_GREEN_PWR output high, JACKET_RED output low, BLADE_GREEN_IN input

	if (!(touch[RED]) && !(touch[GREEN]) ) BUZZ = TRUE;
}
// -------  GREEN inputs finished  --------------------------------------

// foil state machine

	for (player=RED; player <=GREEN; player++) {

	// State machine progression
	switch ( fence[player] ) {
		case Init:
			if ( touch[player] && !THROTTLE ) {
				fence[player] = Transient;
				timer[player] = 0;
				fOntarget[player] = 0;
				microbreak[player] = MICROBREAK_MAX;
			}
			break;
		case Transient:
			if (touch[player]) {
				microbreak[player] = MICROBREAK_MAX;
				if (!offtarget[player]) fOntarget[player]++;
			}
			else {
				microbreak[player]--;
			}
			if ( !microbreak[player] ) {
				fence[player] = Init;
				break;
			}		
			if ( timer[player] > FOIL_DEBOUNCE ) {
				fence[player] = CheckTouch;
			}
			break;
		case CheckTouch:
			fence[player] = Hit;
			if (BUZZ)
			{
				buzzer_on();
				//-TIMER_DDR |= _BV(BUZZER);
				//-TCCRB = _BV(CS21);    // turn on buzzer
				//-TIMER_PORT &= ~(_BV(TIMER_STOP));	// stop timer clock (PD5)
			}
			BUZZ = FALSE;
			timer[player] = 0;
			if ( player == RED ) {
				// LIGHT_PORT |= _BV(RED_LIGHT);
				if (( fOntarget[player]) < (FOIL_DEBOUNCE >>1) ) { 			
					LIGHT_PORT |= _BV(WHITE_RED);
				}
				else LIGHT_PORT |= _BV(RED_LIGHT);
			}
			else {
				// LIGHT_PORT |= _BV(GREEN_LIGHT);
				if (( fOntarget[player]) < (FOIL_DEBOUNCE >>1) ) {
					LIGHT_PORT |= _BV(WHITE_GREEN);
				}
				else LIGHT_PORT |= _BV(GREEN_LIGHT);
			}
			break;
		case Hit:
			if ( timer[player] > FOIL_THROTTLE ) {
				fence[player] = Throttle;
				THROTTLE = TRUE;
			}
			break;
		case Throttle:
			if (!EXTEND_LIGHT) {
				if (timer[player] > BUZZMAX) {
					//-TCCRB = 0;        // turn off buzzer
					buzzer_off();
					timer[player] = 0;
					EXTEND_LIGHT = TRUE;
					//-TIMER_PORT &= ~(_BV(BUZZER));	// set buzzer output to 0
					//-TIMER_DDR &= ~(_BV(BUZZER));
				}
			}
			else {
				if (timer[player] > LIGHTDELAY) {
					EXTEND_LIGHT = FALSE;
					restartStates();
				}
			}
			break;
		}
	}
}


// Process epee events and progress the state machine as required.
// epee - normally open switch; switch closure for 2 - 10 ms establishes a hit
// the blade wire is powered, and if the other blade wire (blade2) 
// is high for 2 ms, then a hit is output
// double hits are possible within a 40 - 50 ms window, otherwise only one
// hit can be recorded
//
void epeeProcess ( void) {

	uint8_t touch[2];
	int8_t player;

	// Set up inputs

    // set guard and one blade wire to output
	INPUT_DDR   = _BV(GUARD_RED)|_BV(GUARD_GREEN); //|_BV(BLADE2_RED)|_BV(BLADE2_GREEN);	// set pullups

	// set guards low - ground guards, output wire to high, set pullups
	INPUT_PORT &= ~(_BV(GUARD_RED)|_BV(GUARD_GREEN));

	BLADE_PWR_DDR = _BV(BLADE_RED_PWR)|_BV(BLADE_GREEN_PWR);
	// power to blades
	BLADE_PWR_PORT = _BV(BLADE_RED_PWR)|_BV(BLADE_GREEN_PWR);

	_delay_us(3);

	// test for grounding on BLADE_X_IN only, BLADE2_X is grounded through resistor
	// need a way to test the other wire
	uint8_t test;

    test = (~INPUT_PIN) & (_BV(BLADE_RED_IN)); //| _BV(BLADE2_RED));
	if (test) LIGHT_PORT |= _BV(YELLOW_RED);
	else LIGHT_PORT &= ~_BV(YELLOW_RED);
    test = (~INPUT_PIN) & (_BV(BLADE_GREEN_IN)); // | _BV(BLADE2_GREEN));
	if (test) LIGHT_PORT |= _BV(YELLOW_GREEN);
    else LIGHT_PORT &= ~_BV(YELLOW_GREEN);

	if (!THROTTLE) {

	// hit if BLADE2 is high - blades wires are connected - tip is pressed
	touch[RED] = INPUT_PIN & _BV(BLADE2_RED);  // input(BLADE2_RED)
	touch[GREEN] = INPUT_PIN & _BV(BLADE2_GREEN);  // input(BLADE2_GREEN)
 	}

	for (player=RED; player <=GREEN; player++) {
		// State machine progression
		switch ( fence[player]) {
			case Init:
				if ( touch[player] && !THROTTLE) {
					fence[player] = Transient;
					timer[player] = 0;
				}
				break;
			case Transient:
			case CheckTouch:
				if ( !touch[player] ) {
					fence[player] = Init;
					break;
				}
				if ( (timer[player] >= EPEE_DEBOUNCE) ) {
					fence[player] = Hit;
					timer[player] = 0;
					buzzer_on();
					//-TIMER_DDR |= _BV(BUZZER);
					//-TCCRB = _BV(CS21);    // turn on buzzer
					//-TIMER_PORT &= ~(_BV(TIMER_STOP));		// stop timer
				}
				break;
			case Hit:
				if ( player == RED ) {
					LIGHT_PORT |= _BV(RED_LIGHT);   // turn on red light
				}
				else {
					LIGHT_PORT |= _BV(GREEN_LIGHT);   // turn on green light
				}
				if ( timer[player] >= EPEE_THROTTLE ) {
					fence[player] = Throttle;
					THROTTLE = TRUE;
				}
				break;
			case Throttle:
			if (!EXTEND_LIGHT) {
				if (timer[player] > BUZZMAX) {
					//-TCCRB = 0;        // turn off buzzer
					buzzer_off();
					timer[player] = 0;
					EXTEND_LIGHT = TRUE;
					//-TIMER_PORT &= ~(_BV(BUZZER));	// set buzzer output to 0
					//-TIMER_DDR &= ~(_BV(BUZZER));
				}
			}
			else {
				if (timer[player] > LIGHTDELAY) {
					EXTEND_LIGHT = FALSE;
					sleeptimer = 0;   //- for automatic shutoff
					restartStates();
				}
			}
			break;
		}			
	}
}


// Process sabre events and progress the state machine  as required.
//
// a hit is determined by contact with the opponents jacket for >0.1ms
// a disconnect occurs when the resistance between blade & guard is > 250 ohms
// for  3 +- 2 ms. (the white lamp is lit).
// blocking is determined as in foil
// a touch can be made on the guard or blade of a player blocking, if the
// increase of resistance through the jacket is < 250 ohms.
// A parry is detected by contact between the blades??
// A hit "through the blade" is registered between 0 and 4 +1 ms after contact
// A hit is not registered (blocked) between 4 and 15 +5 ms after contact,
// provided that contact has not been broken more than 10 times
// A hit is registered normally after this period.
// A touch on the piste is identified first - it would otherwise mask a hit.
//
// rule 5b is still unclear to me but implemented as written
//
// Rule M51 - "A touch made on the metallic strip --- must not be registered
// nor  may  it prevent the registering of a touch made ... by the opponent."
//
// Here, a touch on the piste is checked first - it would otherwise mask a hit.
//
void sabreProcess ( void ) {

	uint8_t test, notouch[2], noparry, piste, player;

	INPUT_DDR  = 0;			 // set all inputs to input mode
	INPUT_PORT = _BV(BLADE_RED_IN);    // set pullup resistor on blade
                                      	   // and tristate all others

    GUARD_PWR_DDR = ~(_BV(GUARD_RED_PWR) | _BV(GUARD_GREEN_PWR));
    GUARD_PWR_PORT = ~(_BV(GUARD_RED_PWR) | _BV(GUARD_GREEN_PWR));

	BLADE_PWR_DDR &= ~(_BV(BLADE_GREEN_PWR));  // turn off  GREEN blade power
	BLADE_PWR_DDR |= _BV(BLADE_RED_PWR);  // turn on  RED blade power

	BLADE_PWR_PORT &= ~(_BV(BLADE_GREEN_PWR)); // tristate GREEN blade power
	BLADE_PWR_PORT |= _BV(BLADE_RED_PWR);  // power RED blade

// BLADE_RED_PWR is on, BLADE_RED_IN is input, all other inputs tri-state
	_delay_us(3);

	piste  = INPUT_PIN & _BV(BLADE_RED_IN);  // input ( BLADE_RED_IN );
	if (!piste) {
		noparry = TRUE;
		notouch[RED] = TRUE;
	}

	else {
	// Detect parry
	INPUT_DDR = _BV(BLADE_GREEN_IN);	// set GREEN blade output (low)

// BLADE_RED_PWR is on, BLADE_RED_IN is input, BLADE_GREEN_IN is output low
	_delay_us(3);

	noparry = INPUT_PIN & _BV(BLADE_RED_IN);  // input ( BLADE_RED_IN );
// player RED
        // Check for unplugged cord -- open between blade & guard

	INPUT_DDR = _BV(GUARD_RED);  // set RED guard to output low

// here, BLADE_RED_PWR is on, BLADE_RED_IN is input, GUARD_RED is output low
	_delay_us(3);

	test = INPUT_PIN & _BV(BLADE_RED_IN);  // input ( BLADE_RED_IN )
        					// if low, RED is unplugged
	if ( !test ) {
		timerOff[RED] = 0;
		sleeptimer = 0;   //- for automatic shutoff
	}

	if ( timerOff[RED] > 30 ) {
		timerOff[RED] = 32;
		LIGHT_PORT |= _BV(WHITE_RED);  // white light (red side)
	}
	else {
		LIGHT_PORT &= ~( _BV(WHITE_RED));  // white light off
	}
	
        // check for blocking - as in foil --- red blade is already powered
        INPUT_DDR = _BV(JACKET_RED);  // set RED jacket to output low
        _delay_us(3);

// here, BLADE_RED_PWR is on, BLADE_RED_IN is input, JACKET_RED is output low
        test = INPUT_PIN & _BV(BLADE_RED_IN);  // input ( BLADE_RED_IN )
        if ( test ) {
                LIGHT_PORT &= ~_BV(YELLOW_RED);  // RED not blocking
        }
        else {
                LIGHT_PORT |= _BV(YELLOW_RED);  // RED is blocking
        }

	// Check for RED hits

	// turn on both BLADE_RED_PWR and GUARD_RED_PWR
		GUARD_PWR_PORT |= _BV(GUARD_RED_PWR);
		GUARD_PWR_DDR |= _BV(GUARD_RED_PWR);

	// Disable player GREEN's weapon, check for hits on him.
		INPUT_DDR = _BV(JACKET_GREEN);  // set GREEN jacket to output low
		_delay_us(3);

// here, BLADE_RED_PWR is on, BLADE_RED_IN is input, JACKET_GREEN is output low
        notouch[RED] = INPUT_PIN & _BV(BLADE_RED_IN); // input(BLADE_RED_IN);
	}

// player GREEN

	INPUT_DDR  = 0;			 // set all inputs to input mode
	INPUT_PORT = _BV(BLADE_GREEN_IN);    // set pullup resistor on blade
                                      	   // and tristate all others
	GUARD_PWR_DDR &= ~(_BV(GUARD_RED_PWR));
	BLADE_PWR_DDR &= ~(_BV(BLADE_RED_PWR));  // turn off red blade
 	BLADE_PWR_DDR |= _BV(BLADE_GREEN_PWR);  // enable GREEN blade power

	GUARD_PWR_PORT &= ~(_BV(GUARD_RED_PWR));
	BLADE_PWR_PORT &= ~(_BV(BLADE_RED_PWR)); // tristate RED blade
	BLADE_PWR_PORT |= _BV(BLADE_GREEN_PWR); // turn on GREEN blade


// BLADE_GREEN_PWR is on, BLADE_GREEN_IN is input, all other inputs tri-state
	_delay_us(3);

	piste  = INPUT_PIN & _BV(BLADE_GREEN_IN);  // input BLADE_GREEN_IN
	if (!piste) {
		noparry = TRUE;
		notouch[GREEN] = TRUE;
	}

	else {
	// check for unplugged cord -- open between blade and guard

        INPUT_DDR = _BV(GUARD_GREEN);  // GREEN guard is set to input low

// BLADE_GREEN_PWR is on, BLADE_GREEN_IN is input, GUARD_GREEN is output low
        _delay_us(3);
	
	test = INPUT_PIN & _BV(BLADE_GREEN_IN);  // input ( BLADE_GREEN_IN )
	if ( !test ) {
		timerOff[GREEN] = 0;
		sleeptimer = 0;   //- for automatic shutoff
	}

	if ( timerOff[GREEN] > 30 ) {
		timerOff[GREEN] = 32;
		LIGHT_PORT |= _BV(WHITE_GREEN); // white light (green)
	}
	else {
		LIGHT_PORT &= ~( _BV(WHITE_GREEN)); //  turn off white light
	}

	// check for GREEN blocking
	INPUT_DDR = _BV(JACKET_GREEN);  // set GREEN jacket to output low
	_delay_us(3);

// BLADE_GREEN_PWR is on, BLADE_GREEN_IN is input, JACKET_GREEN is output low
	test = INPUT_PIN & _BV(BLADE_GREEN_IN);  // input ( BLADE_GREEN_IN )
	if ( test ) {
		LIGHT_PORT &= ~_BV(YELLOW_GREEN); //GREEN is not blocking
	}
	else {
		LIGHT_PORT |= _BV(YELLOW_GREEN); // GREEN is blocking
 	}

	// Check for GREEN hits

// turn on both BLADE_GREEN_PWR and GUARD_GREEN_PWR
		GUARD_PWR_PORT |= _BV(GUARD_GREEN_PWR);
		GUARD_PWR_DDR |= _BV(GUARD_GREEN_PWR);

// Disable player RED's weapon, (done), check for hits on him.
		INPUT_DDR = _BV(JACKET_RED);

// BLADE_GREEN_PWR is on, BLADE_GREEN_IN is input, JACKET_RED is output low
		_delay_us(3);

		notouch[GREEN] = INPUT_PIN & _BV(BLADE_GREEN_IN);

	}

//   -----   all sabre inputs obtained   ---

// A valid hit is >0.1 ms.
// If there is a parry, then a subsequent hit is valid if
//    1) between 0 - 4 ms (+1ms) following the parry
//    2) after 15 ms (+- 5 ms)
//    3) a parried hit is invalid between 4 and 15 ms on condition that
//       contact between the blades is not interrupted more than 10
//       times in that interval
//       
//  Basically, a parry ends after 15 ms (+- 5 ms)
//
// Sabre state machine
//

	for (player=RED; player <=GREEN; player++) {

	switch ( fence[player] ) {
		case Init:
			if ( !notouch[player] && !(THROTTLE || BLOCK)) {
				fence[player] = Transient;
			}
			break;
		case Transient:
			if ( notouch[player] ) {
				fence[player] = Init;
			}
			else {
				fence[player] = CheckTouch;
			}
			break;
		case CheckTouch:
			fence[player] = Hit;
			timer[player] = 0;
			//-TIMER_DDR |= _BV(BUZZER);
			buzzer_on();
			//-TCCRB = _BV(CS21);    			// turn on buzzer
			//-TIMER_PORT &= ~(_BV(TIMER_STOP));	// stop timer
			// turn on red or green light
				if ( player == RED ) {
					LIGHT_PORT |= _BV(RED_LIGHT);   // turn on red light
					NO_RED_HIT = FALSE;
				}
				else {
				LIGHT_PORT |= _BV(GREEN_LIGHT);   // turn on green light
					NO_GREEN_HIT = FALSE;
				}
			break;
		case Hit:
			if ( timer[player] >= SABRE_THROTTLE ) {
				fence[player]   = Throttle;
				THROTTLE = TRUE;
			}
			break;
		case Throttle:
			if (!EXTEND_LIGHT) {
				if (timer[player] > BUZZMAX) {
					buzzer_off();
					//TCCRB = 0;        // turn off buzzer
					timer[player] = 0;
					EXTEND_LIGHT = TRUE;
					//TIMER_PORT &= ~(_BV(BUZZER));	// set buzzer output to 0
					//TIMER_DDR &= ~(_BV(BUZZER));
				}
			}
			else {
				if (timer[player] > LIGHTDELAY) {
					EXTEND_LIGHT = FALSE;
					restartStates();
				}
			}
			break;
		}
	}
	
//-------------------- end for  ---------------------------------------

	switch ( sabreParry) {

		case sParryInit:
			if ( !noparry && !(THROTTLE) ) {
				parrytimer = 0;
				oldparry = noparry;
				sabreParry = sParry;
			}
			break;

		case sParry:
			if (parrytimer >= PARRY_BLOCK_START) {
				sabreParry = sParryBlock;
				oldparry = noparry;
				BLOCK = TRUE;
			}
			break;

		case sParryBlock:
			if (oldparry != noparry) {
				PARRY_INTERRUPT_COUNT++;
				oldparry = noparry;
				if (PARRY_INTERRUPT_COUNT > 20) {
					sabreParry = sParryInit;
					PARRY_INTERRUPT_COUNT = 0;
					BLOCK = FALSE;
				}
			}
			if (parrytimer >= PARRY_BLOCK_END) {
				sabreParry = sParryInit;
				PARRY_INTERRUPT_COUNT = 0;
				BLOCK = FALSE;
			}
			break;
	}

}
                        
void init()
{
	uint8_t test;

// disable watchdog timer
	MCUSR = 0;
	wdt_disable();

// calibrate oscillator
// OSCCAL_calibration();

// set oscillator speed to 8MHz
	clock_prescale_set(clock_div_1);  // logical equivalent to
//	CLKPR = _BV(CLKPCE);
//	CLKPR = 0;    //   8 MHz    1MHz = (_BV(CLKPS1) | _BV(CLKPS0))

// turn off unused modules -- either of the following
/*	power_all_disable();
	power_timer0_enable();
	power_timer1_enable();
	power_timer2_enable(); 
*/
	PRR0 = _BV(PRADC)|_BV(PRUSART0)|_BV(PRSPI)|_BV(PRTWI);

	// Set sleep mode to wait out interrupts
	set_sleep_mode(SLEEP_MODE_IDLE);	  

// Set up timer to generate a square wave for the buzzer
// (The 644 uses timer 2, the 168 uses timer 0 - both are similar
// and set in the include file.)
//
	initialize_ports();

// Set up timer 0 to interrupt every 0.1 ms	
	OCR0A  =  99; 
	TCCR0B = _BV(CS01);		// clk/8 prescaler -- clock = 1MHz
	TCCR0A = _BV(WGM01);    //set CTC mode
	TIMSK0 = _BV(OCIE0A);
	TIFR0 = _BV(OCF0A);  // clear timer flag

	TCCRA = _BV(WGM21)|_BV(COM2A0);    //set CTC mode, toggle OC2A on compare match

	sei();      // enable global interrupts (basically, the timer)

// Mode to write buzzer frequency in eeprom
// Invoked only if FES pin and STATE pin are set to 0 (pressed) on power up
// Output 1 s. at each available frequency to select best sound for buzzer

	MODE_IN_DDR &= ~(_BV(MODE_FES_PIN) | _BV(MODE_STATE_PIN));
	MODE_IN_PORT |= _BV(MODE_FES_PIN) | _BV(MODE_STATE_PIN);  // set pullups
	_delay_us(10);
	test = MODE_IN_PIN & (_BV(MODE_FES_PIN) | _BV(MODE_STATE_PIN));
	if (!test) set_buzz();
	freq = eeprom_read_byte(&ee_buzz); // sets the output half frequency 
	BUZZ_FREQ = freq;

	sleeptimerclock = 0; //- initialize timer clock for timed power off
	sleeptimer = 0;   //- initialize timer for timed power off
	
}

void initialize_ports( void)
{
	TIMER_DDR = _BV(BUZZER)|_BV(TIMER_STOP)|_BV(NOT_F)|_BV(AWAKE);  // DDRD
	TIMER_PORT = _BV(TIMER_BLOCK)|_BV(TIMER_STOP);
	LIGHT_DDR  =  (uint8_t) _BV(RED_LIGHT)|_BV(GREEN_LIGHT)|_BV(WHITE_RED)|_BV(WHITE_GREEN)|_BV(YELLOW_RED)|_BV(YELLOW_GREEN);
	MODE_OUT_DDR |= _BV(MODE_OUT_SABRE) | _BV(MODE_OUT_FOIL);

	switch(mode) 
	{
		case modeEpee:
			MODE_OUT_PORT = _BV(MODE_OUT_SABRE) | _BV(MODE_OUT_FOIL);
			break;

		case modeFoil:
			MODE_OUT_PORT = _BV(MODE_OUT_FOIL);
			break;

		case modeSabre:
			MODE_OUT_PORT = _BV(MODE_OUT_SABRE);
			TIMER_PORT |= _BV(NOT_F);
			break;
	}
}

void test_output()
{

// Test output lights and buzzer
		
// Turn on buzzer
	buzzer_on();
    //-TIMER_PORT |= _BV(BUZZER);
	//-TIMER_DDR  |= _BV(BUZZER);
	//-TCCRB = _BV(CS21);    // clk/8 prescaler -- clock = 1MHz

	LIGHT_PORT &=  ~(_BV(GREEN_LIGHT)|_BV(RED_LIGHT)| _BV(WHITE_GREEN)|_BV(WHITE_RED)| _BV(YELLOW_GREEN)|_BV(YELLOW_RED));
// Turn on lights
	timer[0] = 0;
	LIGHT_PORT |=  _BV(WHITE_GREEN)|_BV(WHITE_RED);
	while (timer[0] <= TESTDELAY) sleep_mode();
	LIGHT_PORT &=  ~(_BV(WHITE_GREEN)|_BV(WHITE_RED));

	timer[0] = 0;
	LIGHT_PORT |=  _BV(GREEN_LIGHT)|_BV(RED_LIGHT);
	while (timer[0] <= TESTDELAY) sleep_mode();
	LIGHT_PORT &=  ~(_BV(GREEN_LIGHT)|_BV(RED_LIGHT));

	timer[0] = 0;
	LIGHT_PORT |= _BV(YELLOW_GREEN)|_BV(YELLOW_RED);
	while (timer[0] <= TESTDELAY) sleep_mode();
	LIGHT_PORT &= ~(_BV(YELLOW_GREEN)|_BV(YELLOW_RED));

// Turn off lights and buzzer
//	LIGHT_PORT =  ~(_BV(RED_LIGHT)|_BV(GREEN_LIGHT)|_BV(WHITE_RED)|_BV(WHITE_GREEN)|_BV(YELLOW_RED)|_BV(YELLOW_GREEN));
	buzzer_off();
	//-TCCRB = 0;             // turn off buzzer
	//-TIMER_PORT &= ~(_BV(BUZZER));	// set buzzer output to 0
	//-TIMER_DDR  &= ~(_BV(BUZZER));
	timer[0] = 0;
}

// restartStates
// Restarts the state machines and turns off all lights and the buzzer
void restartStates ( void ) {
	THROTTLE = FALSE;
	fence[RED]   = Init;
	fence[GREEN] = Init;
	sabreParry   = sParryInit;
	BLOCK = FALSE;
	NO_RED_HIT = TRUE;
	NO_GREEN_HIT = TRUE;

	// Turn the lights and buzzer off
	LIGHT_PORT &= ~(_BV(RED_LIGHT)|_BV(GREEN_LIGHT)|_BV(WHITE_RED)|_BV(WHITE_GREEN)|_BV(YELLOW_RED)|_BV(YELLOW_GREEN));

	// allow timer to run
	TIMER_PORT |= _BV(TIMER_STOP);

	sleep_mode();			// sleep until interrupt
}


ISR (TIMER0_COMPA_vect)
{
	timer[RED]++;
	timer[GREEN]++;
	timerOff[RED]++;
	timerOff[GREEN]++;
	parrytimer++;	
    if (sleeptimerclock++ == 0) sleeptimer++;  //- used for timeout power off
}

void set_mode( void )
{
	uint8_t test, i;
		for (i = 0; i<10; i++) {  // button pressed for 1 ms?		
			test = MODE_IN_PIN & _BV(MODE_FES_PIN);
			if (test) return;
			sleep_mode();
		}		
		if (!test) {
			timer[0] = 0;
			mode = (mode+1)%MAXMODES;
			 
			while(!test) {      // debounce switch
				while (timer[0] < 1000) sleep_mode();		// wait 0.1 s
				test = MODE_IN_PIN & _BV(MODE_FES_PIN);
			}

			TIMER_PORT &= ~(_BV(NOT_F));
			switch(mode) {
			case modeEpee:
				MODE_OUT_PORT = _BV(MODE_OUT_SABRE) | _BV(MODE_OUT_FOIL);
				break;

			case modeFoil:	
				MODE_OUT_PORT = _BV(MODE_OUT_FOIL);
				break;

			case modeSabre:
				MODE_OUT_PORT = _BV(MODE_OUT_SABRE);
				TIMER_PORT |= _BV(NOT_F);
				break;
			}
			restartStates();
		}
		return;
}

inline void buzzer_on(void) {
	TIMER_DDR |= _BV(BUZZER);
	TCCRB = _BV(CS21);    // turn on buzzer
	TIMER_PORT &= ~(_BV(TIMER_STOP));	// stop timer clock (PD5)
}

inline void buzzer_off(void) {
	TCCRB = 0;        // turn off buzzer
	TIMER_PORT &= ~(_BV(BUZZER));	// set buzzer output to 0
	TIMER_DDR &= ~(_BV(BUZZER));
}

void set_buzz ( void ) {
	uint8_t test, i;
	for (i = 0; i<100; i++) {    // make sure buttons are pressed for 10 ms.
		test = MODE_IN_PIN & (_BV(MODE_FES_PIN) | _BV(MODE_STATE_PIN));
		if (test) return;
		sleep_mode();
	}

//	when the MODE_FES_PIN button is pressed as power is switched on,
//	the box produces different frequency buzzer outputs,
//	followed by a 1 second pause
//	press the MODE_FES_PIN button again to select the current tone

	timer[0] = 0;
	while (timer[0] <= 1000) sleep_mode();  // wait 0.1 sec.
	BUZZ_FREQ = freq; // sets the output half frequency 
	//-TCCRB = _BV(CS21);    // turn on buzzer
	buzzer_on();
	while (timer[0] <= 11000) sleep_mode(); // wait 1 sec.	
				
//	use MODE_FES_PIN button to select buzzer frequency
	for (i=255; i>100; i-=5) {
		timer[0] = 0;
		//-TCCRB = 0;             // turn off buzzer
		//-TIMER_PORT &= ~(_BV(BUZZER));	// set buzzer output to 0
		buzzer_off();
		while (timer[0] <= 1000) sleep_mode();  // wait 0.1 sec.
		BUZZ_FREQ  = i; // sets the output half frequency 
		//-TCCRB = _BV(CS21);    // turn on buzzer
		buzzer_on();
		while (timer[0] <= 11000) sleep_mode(); // wait 1 sec.
		test = MODE_IN_PIN & _BV(MODE_FES_PIN);
		if (!test)
		{
			eeprom_write_byte(&ee_buzz, i);
			break;
		}
	}
	//-TCCRB = 0;
	//-TIMER_PORT &= ~(_BV(BUZZER));	// set buzzer output to 0
	//-TIMER_DDR &= ~(_BV(BUZZER));
	buzzer_off();
}

void block()
{
	char test;
// sound buzzer until block stops
	BUZZ_FREQ = freq + 4;
	//-TIMER_DDR |= _BV(BUZZER);
	buzzer_on();
	//-TCCRB = _BV(CS21);    // clk/8 prescaler -- clock = 1MHz
// check to see if external timer is still blocking
	test = TIMER_PIN & _BV(TIMER_BLOCK);
	while (!test)
	{
		test = TIMER_PIN & _BV(TIMER_BLOCK);
		sleep_mode();
	}
	buzzer_off();
	//-TCCRB = 0;		// turn off buzzer
	//-TIMER_PORT &= ~(_BV(BUZZER));	// set buzzer output to 0
	//-TIMER_DDR &= ~(_BV(BUZZER));
	BUZZ_FREQ = freq;
}


// working, current measurement for test board (small leds, 644 processor)
//   in sabre mode -- 29 ma normal mode, 0.4 ua power down mode
// added a new 16 bit timer variable for foil, which works for all
// slows down the interrupt (by 18 clock cycles), though
// needs simulation for the timing tight cases, foil and sabre

void power_down_sleeptimer()     // set sleep mode to power down,
                            // enable external PCInterrupts,
                            // go to sleep
                            // then resets sleep mode on wakeup
                            // and disable PCInterrupts
{
    // flash and buzz to indicate powering down

    test_output();

	//	DISABLE OUTPUTS
	MODE_IN_DDR  = 0;    // DDRA = 0
	MODE_IN_PORT = _BV(MODE_FES_PIN);
	BLADE_PWR_DDR = 0;   // DDRB = 0
	BLADE_PWR_PORT = 0;
	LIGHT_DDR  = 0;      // DDRC = 0
	LIGHT_PORT = 0;
	TIMER_DDR = 0;       //  DDRD = 0;
	TIMER_PORT = _BV(AWAKE);    // weak pullup on awake line

	set_sleep_mode(SLEEP_MODE_PWR_DOWN);
	PCMSK0 = _BV(MODE_FES_PIN); // set interupt mask for FES pin PORT A
	PCIFR |= _BV(PCIF0);        // clear pin change interrupt 0
	PCICR |= _BV(PCIE0);        // enable pin change interrupt 0

	sleep_mode();

	set_sleep_mode(SLEEP_MODE_IDLE);
	PCICR = 0;             // disable pin change interrupt 0
	sleeptimer = 0;        // wait to sleep again
	sleeptimerclock = 0;

	// initialize_ports

	initialize_ports();
	test_output();
}



EMPTY_INTERRUPT (PCINT0_vect)            // wakes up from sleep on pin change

