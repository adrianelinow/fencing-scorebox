
#include <Wire.h>
#include "Adafruit_LEDBackpack.h"

const short GApin = A0, 
            GBpin = A1,
            RApin = A2,
            RBpin = A3; 
            
const short GscrPin = 7,
            GgndPin = 6;
const short RscrPin = 5,
            RgndPin = 4;
const short buzzer = 9;

int Grtn = 0,
    Gwpn = 0,
    Rrtn = 0,
    Rwpn = 0;
    
bool Ghit = false,
     Gprs = false,
     Rhit = false,
     Rprs = false;
long now = 0,
     Gpresstime = 0,
     Rpresstime = 0;
bool lock = false;

#define EPEE 1
#define FOIL 0

const long lockoutTimes[] =   { 300000, 45000 };
const long pressThreshold[] = {  14000,  2000 };


Adafruit_7segment matrix = Adafruit_7segment();

void setup() {
    //Serial.begin(9600);
    //Serial.println("Epee circuit testing");
     
    matrix.begin(0x70);
    matrix.clear();

    pinMode( GscrPin, OUTPUT );
    pinMode( GgndPin, OUTPUT );
    pinMode( RscrPin, OUTPUT );
    pinMode( RgndPin, OUTPUT );
  
    showMode();
    delay(1000);
    signalReady();
}

void showMode(){
    //Serial.println("[DISP] EPEE");
    matrix.print(0xEEEE, HEX);
    matrix.writeDigitRaw(1, B01110011); // P
    matrix.writeDisplay();
}

void loop() {

    while(true){
        
        Grtn = analogRead(GApin);
        Gwpn = analogRead(GBpin);
        //Rrtn = analogRead(RApin);
        //Rwpn = analogRead(RBpin);

        epee();
    
  }

}

void signalResults(){
    if ( not lock ) return;
    signalHalt();
    tone(buzzer, 1000);

    digitalWrite( GscrPin, Ghit);
    digitalWrite( RscrPin, Rhit);

    delay(1000);
    noTone(buzzer);
}

void epee(){
  
    long now = micros();
    
    if( (Ghit && (Gpresstime + lockoutTimes[EPEE] < now)) || (Rhit && (Rpresstime + lockoutTimes[EPEE] < now) ) ){
        lock = true;
        signalResults();
        reset();
        return;
    }
    
    if( not Ghit ){

        if( 400 < Gwpn && Gwpn < 600 && 400 < Grtn && Grtn < 600){
            //Serial.println("PRESSED");

            Gpresstime = micros();
            Gprs = true;
            Ghit = true;
            
        }
    }
    
    /*    
    if( not Ghit )
      if( 400 < Gwpn && Gwpn < 600 && 400 < Grtn && Grtn < 600 ){
          if( not Gprs ){
              Gpresstime = micros();
              Gprs = true;
          }else{
              if ( Gpresstime + pressThreshold[EPEE] <= micros() )
                 Ghit = true;  
          }
      } else {
        if ( Gprs ){
            Gpresstime = 0;
            Gprs = false;  
        }  
    }//*/
}

void signalHalt(){
  // HALT
    matrix.writeDigitRaw(0, B1110110); // H 
    matrix.writeDigitRaw(1, B1110111); // A
    matrix.writeDigitRaw(3, B0111000); // L
    matrix.writeDigitRaw(4, B1111000); // T
    matrix.writeDisplay();
}

void signalReady(){
    // ALLE
    matrix.writeDigitRaw(0, B1110111); // A
    matrix.writeDigitRaw(1, B0111000); // L
    matrix.writeDigitRaw(3, B0111000); // L
    matrix.writeDigitRaw(4, B1111001); // E
    matrix.writeDisplay();
}

void reset(){

    // reset variables
    lock = false;
    
    Ghit = false;
    Gprs = false;
   
    Gwpn = 0;
    Grtn = 0; 
    Gpresstime = 0;
    
    // reset output
    digitalWrite(GscrPin, LOW);
    digitalWrite(RscrPin, LOW);

    delay(1000);
    signalReady();
    
    
}
