
#include <Wire.h>
#include "Adafruit_LEDBackpack.h"

#define DEBUG true

const short GApin = A0,
            GBpin = A1,
            RApin = A2,
            RBpin = A3;

const byte GscrPin = 7,
           GgndPin = 6;
const byte RscrPin = 5,
           RgndPin = 4;
const byte buzzer = 9;

short Grtn = 0,
      Gwpn = 0,
      Rrtn = 0,
      Rwpn = 0;
      
long timestamps[] = { 0, 0, 0};
// { current time, Green press time, Red press time }
/* triggerFlag: stores which fencer touched first.
      0 | neither
      1 | green
      2 | red
      3 | both
*/
#define NONE 0
#define GREEN 1
#define RED 2
#define BOTH 3

byte touchFlag = NONE;
byte eval = NONE;

bool lock = false;

#define EPEE 1
#define FOIL 0

byte mode = EPEE;

const long lockoutTimes[] =   { 300000, 45000 };
const long pressThreshold[] = {  14000,  2000 };


Adafruit_7segment matrix = Adafruit_7segment();

void setup() {

  Serial.begin(9600);
  Serial.println("Start");

  matrix.begin(0x70);
  matrix.clear();

  pinMode( GscrPin, OUTPUT );
  pinMode( GgndPin, OUTPUT );
  pinMode( RscrPin, OUTPUT );
  pinMode( RgndPin, OUTPUT );

  showMode();
  delay(1000);
  signalReady();
}

void showMode() {

  if ( mode == EPEE ) {
    matrix.print(0xEEEE, HEX);
    matrix.writeDigitRaw(1, B01110011); // P
    matrix.writeDisplay();
    return;
  }
  if ( mode == FOIL ) {
    // write FOIL to matrix
    return;
  }
}

void loop() {
  
  while (true) {

    if ( lock ) {
      if( DEBUG ) Serial.println("[LOCK] Signalling results + reset");
      signalResults();
      reset();
      continue;
    }

    Grtn = analogRead(GApin);
    Gwpn = analogRead(GBpin);
    Rrtn = analogRead(RApin);
    Rwpn = analogRead(RBpin);

    epee();

  }

}

void signalResults() {
  if ( not lock ) return;
  signalHalt();
  tone(buzzer, 1000);

  if( DEBUG ){
    Serial.println("\tSignaling Results!!!");
    Serial.print("EVAL = ");
    Serial.println(eval);
  }
 
  if( eval == BOTH ){
    digitalWrite( GscrPin, HIGH );
    digitalWrite( RscrPin, HIGH ); 
  }else if( eval == RED )
    digitalWrite( RscrPin, HIGH );
   else if( eval == GREEN )
    digitalWrite( GscrPin, HIGH );

  delay(1000);
  noTone(buzzer);
}

byte evalSplitEpee() {
  if( DEBUG ) Serial.println("evaluating split");
  // returns true if split-time is within threshold of locktime

    if( DEBUG ){ 
        Serial.println("State:");
        Serial.print("\tTS[GREEN] = ");
        Serial.println(timestamps[GREEN]);
        Serial.print("\tTS[RED] = ");
        Serial.println(timestamps[RED]);
    }
    // if one timestamp is zero (no touch), return the non-zero one
    if( min( timestamps[GREEN], timestamps[RED] ) == 0 )
        return ( ( not timestamps[GREEN] ) ? RED : GREEN );
    // since both are non-zero, if the difference is within lockout, return BOTH
    if( lockoutTimes[EPEE] > max( timestamps[GREEN], timestamps[RED] ) - min( timestamps[GREEN], timestamps[RED]) )
        return BOTH;
    // otherwise, return smaller (first) one
    return ( (timestamps[GREEN] < timestamps[RED]) ? GREEN : RED);

}

byte epee() {

  long now = micros();

  //if( DEBUG ) Serial.println("-");

    if( touchFlag && (now - timestamps[touchFlag] > lockoutTimes[EPEE])  ){
        if( DEBUG ) Serial.println("locking!");
        lock = true;
        eval = evalSplitEpee();
    }  

   if ( ( 400 < Gwpn ) && (Gwpn < 600) && ( 400 < Grtn ) && (Grtn < 600) ) {
        timestamps[GREEN] = now;
        if( not touchFlag ) touchFlag = GREEN;
    }

    if ( ( 400 < Rwpn ) && (Rwpn < 600) && ( 400 < Rrtn ) && (Rrtn < 600) ) {
        timestamps[RED] = now;
        if( not touchFlag ) touchFlag = RED;
    }

}

void signalHalt() {
  // HALT
  matrix.writeDigitRaw(0, B1110110); // H
  matrix.writeDigitRaw(1, B1110111); // A
  matrix.writeDigitRaw(3, B0111000); // L
  matrix.writeDigitRaw(4, B1111000); // T
  matrix.writeDisplay();
}

void signalReady() {
  // ALLE
  matrix.writeDigitRaw(0, B1110111); // A
  matrix.writeDigitRaw(1, B0111000); // L
  matrix.writeDigitRaw(3, B0111000); // L
  matrix.writeDigitRaw(4, B1111001); // E
  matrix.writeDisplay();
}

void reset() {

  // reset variables
  lock = false;

  Gwpn = 0;
  Grtn = 0;

  Rwpn = 0;
  Rrtn = 0;

  touchFlag = NONE;

  timestamps[0] = 0;
  timestamps[1] = 0;
  timestamps[2] = 0;

  // reset output
  digitalWrite(GscrPin, LOW);
  digitalWrite(GgndPin, LOW);
  digitalWrite(RscrPin, LOW);
  digitalWrite(RgndPin, LOW);

  delay(1000);
  signalReady();

}
